<?php
/**
 * Author:          Andrea Milone
 * Created on:      11/02/2020
 *
 * @package Neve
 */

get_header();

?>
	<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php the_title() ?></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
    <div class="d-flex">
        <figure alt="" class="primary-dimen pad-10">
			<?php the_post_thumbnail('example') ?>
		</figure>
        <div class="primary-dimen d-flex flex-wrap">
            <div class="first-info-dimen d-flex ">
                <div class="d-flex sub-first-info-dimen">
                    <div class="d-flex info-sub-first-info">
                        <i class="material-icons icon-info-sub-first-info">location_on</i> 
                        <div class="text-info-sub-first-info d-flex"><?php the_tags() ?></div>
                    </div>
                </div>
                <div class="d-flex sub-first-info-dimen">  
                        <div class="text-info-sub-first-info d-flex">
							<?php the_category();?>
                    </div>
                </div>
                <div class="d-flex sub-first-info-dimen">
                    <div class="d-flex info-sub-first-info">
                        <i class="material-icons icon-info-sub-first-info">perm_contact_calendar</i> 
                        <div class="text-info-sub-first-info d-flex">
							<?php echo get_the_date() ?></div>
                    </div>
                </div>
            </div>
            <div class="second-info-dimen ">
                <a href="#" class="fa fa-facebook ml-icon-social-single-post icon-social-single-post"></a>
                <a href="#" class="fa fa-twitter icon-social-single-post"></a>
                <a href="#" class="fa fa-instagram icon-social-single-post"></a>
            </div>
            <div class="third-info-dimen ">
                <h3 class="txt-center"><?php echo get_the_author(); ?></h3>
				<h2 class="txt-center"><?php the_title(); ?></h2>
            </div>
            <div class="fourth-info-dimen  d-flex">
                <audio controls>
                    <source src="" type="audio/mpeg">
                </audio>
            </div>
            <div class="fifth-info-dimen ">
                <p class="pad-10">
                   <?php 
					 the_content();
					?>
                </p>
            </div>
        </div>
    </div>
</body>
	<?php get_footer(); ?>
	
    <style>

        .d-flex {
            display: flex;
        }


        .primary-dimen {
            width: 50%;
            height: auto;
        }

        .flex-wrap {
            flex-wrap: wrap;
        }

        .pad-10 {
            padding: 10px;
        }

        .txt-center {
            text-align: center;
        }

        .first-info-dimen {
            width: 100%; 
            height: 100px; 
            flex-wrap: nowrap; 
            justify-content: space-between;
        }

        .second-info-dimen {
            width: 100%; 
            height: 50px; 
            margin-top: 20px; 
            margin-bottom: 20px;
        }

        .third-info-dimen {
            width: 100%; 
            height: auto; 
            margin-top: 20px; 
            margin-bottom: 20px;
        }

        .fourth-info-dimen {
            width: 100%; 
            height: auto; 
            margin-top: 20px; 
            margin-bottom: 20px;
            justify-content: center;
            align-items: center;
        }

        .fifth-info-dimen {
            width: 100%; 
            height: auto; 
            margin-top: 20px;
        }

        .sub-first-info-dimen {
            width: 30%; 
            height: auto;  
            justify-content: center; 
            align-items: center;
        }

        .info-sub-first-info {
            border: 2px solid black;  
            flex-wrap: nowrap;
        }

        .icon-info-sub-first-info {
            margin-left: 5px; 
            margin-right: 10px; 
            font-size: 40px;
        }

        .text-info-sub-first-info {
            font-size: 20px;
            align-items: center; 
            margin-right: 5px;
        }

        .icon-social-single-post {
            margin-left: 50px; 
            font-size: 45px; 
            color: black; 
            text-decoration: none;
        }

        .ml-icon-social-single-post {
            margin-left: 100px;
        }

    </style>
</html>
<?php
get_footer(); ?>
