<?php
/**
 * Author:          Andrea Milone, Simone Tugnetti
 * Created on:      11/02/2020
 *
 * @package Neve
 */

get_header();

?>
<html lang="it">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php the_title() ?></title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>
    <body>
        <div class="container-body p-30 bgc-body-single-post">
            <figure alt="" class="primary-dimen pad-25 d-flex ju-align-center">
                <?php the_post_thumbnail('full') ?>
		    </figure>
            <div class="primary-dimen d-flex flex-wrap">
                <div class="first-info-dimen d-flex">
                    <div class="d-flex sub-first-info-dimen ju-align-center">
                        <div class="d-flex info-sub-first-info pad-10 bgc-white">
                            <div class="w-100 txt-center">
                                <i class="material-icons icon-info-sub-first-info">location_on</i>
                            </div>
                            <div class="text-info-sub-first-info txt-center">
                                <?php the_tags() ?>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex sub-first-info-dimen ju-align-center">
                        <div class="d-flex info-sub-first-info pad-10 bgc-white">    
                            <div class="text-info-sub-first-info txt-center">
                                <?php the_category();?>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex sub-first-info-dimen ju-align-center">
                        <div class="d-flex info-sub-first-info pad-10 bgc-white">
                            <div class="w-100 txt-center">
                                <i class="material-icons icon-info-sub-first-info">perm_contact_calendar</i>
                            </div>
                            <div class="text-info-sub-first-info txt-center">
                                <?php echo get_the_date() ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="second-info-dimen m-top-bott d-flex ju-align-center">
                    <a href="#" class="fa fa-facebook icon-social-single-post"></a>
                    <a href="#" class="fa fa-twitter icon-social-single-post"></a>
                    <a href="#" class="fa fa-instagram icon-social-single-post"></a>
                </div>
                <div class="third-info-dimen m-top-bott">
                    <h3 class="txt-center"><?php echo get_the_author(); ?></h3>
				    <h2 class="txt-center"><?php the_title(); ?></h2>
                </div>
                <div class="fourth-info-dimen d-flex m-top-bott ju-align-center">
                    <audio controls>
                        <source src="" type="audio/mpeg">
                    </audio>
                </div>
                <div class="fifth-info-dimen">
                    <p class="pad-10 d-flex ju-align-center">
                        <?php the_content(); ?>
                    </p>
                </div>
            </div>
        </div>
    </body>
	    <?php get_footer(); ?>
	
        <style>

            .container-body {
                display: flex;
            }

            .bgc-body-single-post {
                background-color: #f1f1f1;
            }

            .bgc-white {
                background-color: white;
            }

            .ju-align-center {
                justify-content: center;
                align-items: center;
            }

            .d-flex {
                display: flex;
            }

            .w-100 {
                width: 100%;
            }

            .p-30 {
                padding: 30px;
            }

            .pad-25 {
                padding: 25px;
            }

            .primary-dimen {
                width: 50%;
                height: auto;
            }

            .flex-wrap {
                flex-wrap: wrap;
            }

            .pad-10 {
                padding: 10px;
            }

            .txt-center {
                text-align: center;
            }

            .m-top-bott {
                margin-top: 40px; 
                margin-bottom: 40px;
            }

            .first-info-dimen {
                width: 100%; 
                height: auto; 
                flex-wrap: nowrap; 
                justify-content: space-between;
            }

            .second-info-dimen {
                width: 100%; 
                height: 50px;
            }

            .third-info-dimen {
                width: 100%; 
                height: auto; 
            }

            .fourth-info-dimen {
                width: 100%; 
                height: auto;
            }

            .fifth-info-dimen {
                width: 100%; 
                height: auto; 
                margin-top: 20px;
            }

            .sub-first-info-dimen {
                width: 30%; 
                height: auto;
            }

            .info-sub-first-info {
                border: 2px solid black;  
                flex-wrap: wrap;
            }

            .icon-info-sub-first-info {
                font-size: 40px;
            }

            .text-info-sub-first-info {
                font-size: 20px;
                width: 100%;
            }

            .icon-social-single-post {
                margin-left: 50px; 
                font-size: 45px; 
                color: black; 
                text-decoration: none;
            }

            .icon-social-single-post:hover { 
                color: #EFA913; 
                text-decoration: none;
            }

            @media screen and (max-width: 800px) {

                .primary-dimen {
		            width: 100%;
	            }
	
	            .container-body {
		            flex-wrap: wrap;
	            }

            }

            @media screen and (max-width: 480px) {
                
                .sub-first-info-dimen {
                    width: 100%;
                    margin-top: 20px;
                }

                .first-info-dimen {
                    flex-wrap: wrap;
                }

            }


        </style>
</html>
<?php
get_footer(); 
?>
