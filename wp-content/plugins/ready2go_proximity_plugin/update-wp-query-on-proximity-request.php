<?php
// file made by Gregoricchio Matteo

// takes @args parameter from apply_filter() and modifies some array keys value
// to update the WP_QUERY used by the plugin wp-show-posts.
function update_WP_QUERY_args ($args){

        $getgeolocationkeys = order_by_geolocation();

        if ($args['orderby'] == 'location'){

                $args['geo_query'] = $getgeolocationkeys['geo_query'];

                 if (array_key_exists('order', $args)){
                        $args['order'] = $getgeolocationkeys['order'];
                }       else {
                        array_push($args, $getgeolocationkeys['order']);
                }
                if (array_key_exists('orderby', $args)){
                        $args['orderby'] = $getgeolocationkeys['orderby'];
                }       else {
                        array_push($args, $getgeolocationkeys['orderby']);
                }

        }
        return $args;
}

// geo-location keys used in a WP_QUERY to realize a sort by proximity query.
function order_by_geolocation (){

	$lat = isset($_SESSION['lat'] ) ? (int)$_SESSION['lat'] : 45.063311;
        $long = isset($_SESSION['long'] ) ? (int)$_SESSION['long'] : 7.681860;

        $arg =  array(
                'geo_query' => array(
                        'lat_field' => '_ready2go_latitude',  // this is the name of the meta field storing latitude
                        'lng_field' => '_ready2go_longitude', // this is the name of the meta field storing longitude
                        'latitude'  => $lat,    // this is the latitude of the point we are getting distance from
                        'longitude' => $long,   // this is the longitude of the point we are getting distance from
                        'units'     => 'miles'       // this supports options: miles, mi, kilometers, km
                ),
                'orderby' => 'distance', // this tells WP Query to sort by distance
                'order'   => 'ASC'
                );
        return $arg;
}

/*
 * filter called before the WP_QUERY object is created in WP Show Posts plugin List. Updates the wp-query arguments of wp-show-posts
 */
add_filter('wp_show_posts_shortcode_args', 'update_WP_QUERY_args', 12, 1);

?>
