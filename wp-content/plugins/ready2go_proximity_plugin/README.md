# Ready2Go Plugin - README.md
**Readme ufficiale del progetto Pillole di Armando (PdA) - team Ready2Go - Laboratorio Integrato anno 2020 - ITS ICT Piemonte**
Sito Progetto: https://ready2go.its2020.tk/
Repository ufficiale del progetto: https://gitlab.com/ready2go_itsturin2020/ready2go_pda
## Setup
#### Plugin Setup
È richiesta l'installazione dei seguenti plugins:
- [OSM](https://it.wordpress.org/plugins/osm/)
- [WP Show Posts](https://it.wordpress.org/plugins/wp-show-posts/)
- [ResponsiveVoice Text to Speech] (https://it.wordpress.org/plugins/responsivevoice-text-to-speech/)

Il plugin WP Query Geo, incluso nel plugin del gruppo, deve essere attivato per il corretto funzionamento del main plugin.

Nel file *set-user-location-data.php* alla riga 42 inserire la propria chiave privata (ottenuta tramite iscrizione al servizio [LocationIQ](locationiq.com)).

#### Setup del Form di Contatto
1. Nelle impostazioni del plugin WP Mail SMTP, sotto General/Mailer impostare il mailer scelto per la gestione delle email inviate dagli utenti.
Nel progetto è stato scelto GMail, seguendo queste istruzioni: https://wpmailsmtp.com/docs/how-to-set-up-the-gmail-mailer-in-wp-mail-smtp/
2. Da wp-admin: andare in Contatto/"Moduli Di Contatto", aprire in modifica il modulo "Invia la Tua Storia". Nella tab Mail modificare il parametro "A:", indicando la mail a cui si desidera ricevere i form inviati dagli utenti.

## Istruzioni d'uso
#### Come GeoTaggare Un Post
In un articolo, andare nella sezione "WP OSM Plugin shortcode generator", scegliere la tab "Set GeoTag".
Selezionare un'icona tra le opzioni, poi nella mappa sottostante cercare il luogo desiderato e cliccarci sopra.
Una volta che l'icona è comparsa sulla mappa, premere il bottone "Salva" sottostante. Una volta eseguite queste operazioni, salvare il post (come bozza o come articolo pubblicato).
Dopo aver creato un geotag per un articolo, vengono aggiunti in automatico 2 tag relativi alla posizione scelta, città e nazione (se trovati rispetto al geotag scelto).
NB: se si aggiorna la posizione di un articolo, si prega di eliminare dai tag le precedenti città/nazioni inserite, per non esporre informazioni scorrette agli utenti.
Vedi anche: https://media.giphy.com/media/Su1CWqYUelrarzgZQ1/giphy.gif

#### ResponsiveVoice Shortcodes
Per inserire un bottone per la lettura vocale di un contenuto testuale, usare lo shortcode:
[responsivevoice_button voice="Italian Female"]
Perché il bottone legga solo parte di un testo, inserire il contenuto testuale all'interno di questo shortcode:
[responsivevoice voice="Italian Female"] ---content_placeholder---[/responsivevoice]

#### Google Analytics
Per visualizzare unanteprima del report di google analytics recarsi sulla bacheca di wordpress, lì sarà presente una finestra chiamata mosterninsights che mostrerà i principali dati sul monitoraggio delle pagine. 
Per una visione più dettagliata andare sul sito di google analytics e loggarsi con una delle email registrate, (i tre mentor) e li si potrà usare il servizio completo. 
Per visualizzare i dati in tempo reale è necessario che un utente non admin stia navigando sul sito, e li si potranno osservare le action che lutente effettua.

Buon divertimento!

