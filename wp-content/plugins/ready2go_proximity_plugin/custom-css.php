<?php
// file made by Tugnetti Simone, Milone Andrea
add_action( 'wp_head', function () { ?>
	<style>
		.mostrabottoni button.active {
		  background-color: #25A9B4;
		  color: white;
		}
		.mostrabottoni button, .r2g_btn {
		  background-color: white;
		  color: black;
		}
		.r2g_myBtn {
			width: 100%;
			font-size: 20px;
			border-radius: 3px;
			padding:10px;
		}
		.r2g_footer {
		  position: fixed;
		  left: 0;
		  bottom: 0;
		  width: 100%;
		  padding: 10px;
		  color: white;
		  text-align: center;
		}
	</style>
<?php } );

