// file made by Gregoricchio Matteo

// get user location from HTML5 GeoLocation API
function getLocation() {
	if (navigator.geolocation) {
        	navigator.geolocation.getCurrentPosition(success, showError);
	} else {
        	alert("Geolocation is not supported by this browser.");
    	}
}
function success(pos) {
  	var crd = pos.coords;
	let setLocation;
	fetch("https://nominatim.openstreetmap.org/reverse?format=geocodejson&lat="+crd.latitude+"&lon="+crd.longitude)
	.then((res) => res.json())
	.then((data) => {
		setLocation = data.features[0].properties.geocoding.label;
	}).catch((error) => {setLocation('There\'s been an error, try again');});
	jQuery.post(
		ajaxreq.url,
	  	{
			'action': 'set_geo_data',
			'lat' : crd.latitude,
			'long' : crd.longitude,
		},
		function(response){
			if (response=='ok'){
				jQuery('.setting_result').html('<div><strong>GeoData Acquired: ' + setLocation + '</strong></div>');
			} else {
				jQuery('.setting_result').html('<div><strong>Error retrieving your position. Please set it manually.</strong></div>');
			}
			console.log('data acquired');
		}
	);
}
function showError(error) {
	switch(error.code) {
		case error.PERMISSION_DENIED:
      			alert("User denied the request for Geolocation.");
      			break;
    		case error.POSITION_UNAVAILABLE:
      			alert("Location information is unavailable.");
      			break;
    		case error.TIMEOUT:
      			alert("The request to get user location timed out.");
      			break;
    		case error.UNKNOWN_ERROR:
      			alert("An unknown error occurred.")
      			break;
	}
}

jQuery('document').ready(() => {

	// retrieve user location from user text input
        jQuery('.r2g_user_set').on('click', (e) => {
                        let query = jQuery(e.target).siblings('input').val();
                        jQuery.ajax({
                                url : ajaxreq.url + "?search="+query,
                                type : "GET",
                                data : {
                                        'action' : 'set_geo_from_user'
                                }
                        }).done( (response) => {jQuery('.setting_result').html('<div><strong>The location you set is: '+response+'</strong></div>')});
        })

	if (jQuery('#r2g_sortby_data').length == 0) {
		jQuery("div #myBtn").hide();
	}

	// append footer for kebab gallery menu
	if (jQuery('#r2g_sortby_data').length !=0){
		jQuery('body').append('<div class="r2g_footer"><button class="r2g_myBtn">Show Menu Search</button></div>');
		jQuery(".r2g_footer").hide();

		if (jQuery(window).width() < 1024) jQuery(".r2g_footer").show();
		jQuery(window).on('resize', () => {
			jQuery(window).width() < 1024 ? jQuery(".r2g_footer").show() : jQuery('.r2g_footer').hide();
		})
	}

	// hide/show sidebar based on media query
 	if (jQuery(window).width() < 1024){
                jQuery('div .blog-sidebar').hide();
                jQuery('.neve-main>.container .col').css('max-width', '99%');
        }
	jQuery(window).on('resize', () => {
        if (jQuery(window).width() < 1024){
                jQuery('div .blog-sidebar').hide();
                jQuery('.neve-main>.container .col').css('max-width', '99%');
        } else {
               jQuery('div .blog-sidebar').show();
                jQuery('.neve-main>.container .col').css('max-width', '80%');
	}
	});

	// hide/show overlay menu
 	jQuery(".r2g_myBtn").on ('click', (event) => {
		jQuery('#myBtn').trigger('click');
	})

	// add correct background-color to the sidebar
	jQuery('.nv-sidebar-wrap').css('background-color', '#EFA913');

	// on gallery pages: select active buttons in fly menu based on window.location
	if (jQuery('.r2g-sortby').length != 0) {
		var pathArray = window.location.pathname.split('/');
		let values = pathArray[3] != undefined ? pathArray[3].split('-') : '';
			if (values[0] == 'map') {
				jQuery('.r2g-sortby i').attr('value', 'map');
				jQuery('.r2g-sortby i').removeClass('fa-toggle-off');
				jQuery('.r2g-sortby i').addClass('fa-toggle-on');

			}
			if (values[0] == 'data' || values[0] == '' || values[0] == 'page') {
			        jQuery('#r2g_sortby_data').closest('.elementor-button-link').css('background-color', 'orange');
			} else if (values[0] == 'location') {
                                jQuery('#r2g_sortby_location').closest('.elementor-button-link').css('background-color', 'orange')
                        }
        	if (values[1] != '') {
			let activebutton = jQuery('.mostrabottoni button#' +values[1]);
			activebutton.addClass('active');
		};
	}

	// on gallery page: redirect to correct page based on the click on Gallery/Map button in the fly menu
        jQuery('.r2g-sortby').on('click', (event) => {
		if (jQuery(event.target).attr('value') == 'map'){
	               jQuery(location).attr('href', "/index.php/racconti/");
		} else {
                       jQuery(location).attr('href', "/index.php/racconti/map");
		}
		return false;
	})

        // on gallery page: redirect to correct page requested, based on the click on 'sort by data' button in page
	jQuery('#r2g_sortby_data').on('click', (event) => {
		event.preventDefault();
		orderByRequest('data');
	})
	 // on gallery page: redirect to correct page requested, based on the click on 'sort by location' button in page
        jQuery('#r2g_sortby_location').on('click', (event) => {
                event.preventDefault();
                orderByRequest('location');
        })

	function orderByRequest (element) {
                let active_btn = jQuery('.mostrabottoni button').hasClass("active") ? '-' + jQuery('.mostrabottoni button.active').attr('id'): '';
		let sortby = element;
                let query = active_btn != '' ? sortby + active_btn : sortby == 'data' ? '' : 'location';
                console.log(jQuery(location).attr('href', "/index.php/racconti/"+ query + "/"));
                return false;
	}

        // on gallery page: redirect to correct page requested, based on the click on category button in the fly menu
	jQuery('.mostrabottoni button').bind('click', (event) => {
		if (jQuery('.r2g-sortby').attr('value') == 'map'){jQuery(location).attr('href', "/index.php/racconti/"); return false;}
		let active_btn = '';
		if (jQuery(event.target).hasClass('active')){
			 jQuery('.mostrabottoni').children('button').removeClass('active');
		} else {
			jQuery('.mostrabottoni').children('button').removeClass('active');
                	jQuery(event.target).toggleClass('active');
	                active_btn = '-' + jQuery(event.target)[0].id;
		}
		var pathArray = window.location.pathname.split('/');
                let values = pathArray[3].split('-');
		let sortby =  values[0] == 'location' ? 'location' : active_btn == '' ? 'racconti' : 'data';
		let query = sortby + active_btn;
		jQuery(location).attr('href', "/index.php/racconti/"+query + "/");
		return false;
	});
})
