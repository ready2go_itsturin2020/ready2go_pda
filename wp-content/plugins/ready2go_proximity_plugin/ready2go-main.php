<?php
/*
Plugin Name: Sort By Post GeoLocation Proximity
Plugin URI: https://ready2go.its2020.tk
Description: Get GeoLocation Data from OSM Plugin and sort posts by proximity, using Greg Schoppe Plugin Solution. Please note this plugin REQUIRES to work: OSM, wp-query-geo plugin (Greg Schppe, included in this plugin), WP Show Posts. Please Note 2: On plugin activation the wp-query-geo gets automatically activated.
Version: 2.0
Author: matteo-gregoricchio, edoardo-presutti (part of ready2go-ITS2020-Team)
Author URI: https://github.com/followynne
*/

// file made by Gregoricchio Matteo, Presutti Edoardo

// No direct access, please
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// plugin files include
include_once ( plugin_dir_path( __FILE__ ) . 'my-own-shortcodes.php');
include_once ( plugin_dir_path( __FILE__ ) . 'create-autogeo-tags.php');
include_once ( plugin_dir_path( __FILE__ ) . 'update-wp-query-on-proximity-request.php');
include_once ( plugin_dir_path( __FILE__ ) . 'add-options-to-wp-show-posts.php');
include_once ( plugin_dir_path( __FILE__ ) . 'set-user-location-data.php');
include_once ( plugin_dir_path( __FILE__ ) . 'custom-css.php');

// requirements setup
register_activation_hook( __FILE__, 'r2g_requirements');

function r2g_requirements () {
	$res = true;
	activate_plugin(  plugin_dir_path( __FILE__ ) . 'wp-query-geo.php');

	if ( ! is_plugin_active(  'ready2go_proximity_plugin/wp-query-geo.php') ) {
	        $res = false;
	}
	if ( ! is_plugin_active( 'osm/osm.php') ) {
		$res = false;
	}
	if ( ! is_plugin_active( 'wp-show-posts/wp-show-posts.php') ) {
                $res = false;
        }
	if ( !$res ){
      		die('<div class="error"><p>To activate this plugin, you need to activate the following plugins:'.
		    '<ul><li>OSM</li><li>WP Geo Query</li><li>WP Show Posts</ul></p></div>');
	}
}

// start browser session 
function register_session(){
    if( !session_id() )
        session_start();
}

add_action('init','register_session');

// add javascript file
add_action( 'wp_enqueue_scripts', 'scripts_r2g' );

function scripts_r2g () {
	wp_enqueue_script('r2g_main_v2', plugin_dir_url( __FILE__ ) .'r2g_main.js', array('jquery'), time(), true);
	wp_localize_script('r2g_main_v2', 'ajaxreq',
			 array('url' => admin_url('admin-ajax.php')
	));
}

// deactivation operations

register_deactivation_hook( __FILE__, 'deactivate_wpquerygeo');

function deactivate_wpquerygeo () {
	$dependent = 'ready2go_proximity_plugin/wp-query-geo.php';
	deactivate_plugins( $dependent);
    	if( ! is_plugin_active($dependent) ){
         add_action('update_option_active_plugins', 'deactivate_second_time');
    }
}

function deactivate_second_time (){
	$dependent = 'ready2go_proximity_plugin/wp-query-geo.php';
	deactivate_plugins($dependent);
}

?>
