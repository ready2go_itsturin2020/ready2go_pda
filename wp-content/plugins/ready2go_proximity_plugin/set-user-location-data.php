<?php
// file made by Gregoricchio Matteo

// set coordinates values from geolocation trigger
add_action('wp_ajax_set_geo_data', 'set_coordinates');
add_action('wp_ajax_nopriv_set_geo_data', 'set_coordinates');

function set_coordinates () {

        $_SESSION['lat'] = (isset($_POST['lat'])) ? $_POST['lat'] :'';
        $_SESSION['long'] =(isset($_POST['long']))?$_POST['long']:'';
	if ($_SESSION['lat']!= '') echo 'ok';
        wp_die();
}

// get location from query string sent from user and set coordinates values
add_action('wp_ajax_set_geo_from_user', 'retrieve_then_set_coordinates');
add_action('wp_ajax_nopriv_set_geo_from_user', 'retrieve_then_set_coordinates');

function retrieve_then_set_coordinates () {
	if (isset($_GET['search'])){
		$data = array(
		  CURLOPT_RETURNTRANSFER    =>  true,
		  CURLOPT_FOLLOWLOCATION    =>  true,
		  CURLOPT_MAXREDIRS         =>  10,
		  CURLOPT_TIMEOUT           =>  30,
		  CURLOPT_CUSTOMREQUEST     =>  'GET',
		);

		$curl = wp_remote_request('https://us1.locationiq.com/v1/search.php?key=pk.7a7310d1525cb4f52050052961dda5d1&q='.$_GET['search'].'&format=json', $data);
		$body = wp_remote_retrieve_body($curl);
		$response = json_decode($body);

		if (is_array($response)){ 
			$res = (array)$response[0];
			$_SESSION['lat'] = (isset($res['lat'])) ? $res['lat'] :'';
			$_SESSION['long'] =(isset($res['lon']))?$res['lon']:'';
	                print_r( $res['display_name']);
		} else {
			echo ' Error';
		}
		wp_die();
	}
}
?>
