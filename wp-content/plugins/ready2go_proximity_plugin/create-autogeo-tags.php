<?php
// file made by Gregoricchio Matteo
include_once (plugin_dir_path( __FILE__ ).'../osm/osm.php');

// on post save, it updates the current tags with city/region info, based on the story geo-tag.
add_action('save_post','add_tags_location');

/* 1. takes longitude & latitude from OSM Geo-Tagging, add them as two separate metakeys
 * that will be used for the promixity sort.
 * 2. makes a get request to openstreetmap reverse geocoding API to retrieve location data
 * based on GeoTag. If city and region exist in the API response, it adds them as post tags.
 * 3. add the city/region as a meta key for the post, to be displayed in the single post page.
 */
function add_tags_location($post_id) {
	$post = get_post($post_id);
	if(!$post instanceOf WP_Post) return;

	if ($post->post_type=='post'){
    	  if (function_exists(OSM_echoOpenStreetMapUrl)) {
        	if ((OSM_getCoordinateLong('osm'))&&(OSM_getCoordinateLat('osm'))) {
                	$val = [OSM_getCoordinateLat('osm'), OSM_getCoordinateLong('osm')];
			wp_add_metakeys($val, $post);
        	        $data = get_location($val);
                	wp_set_post_tags($post->ID, $data, true);
			$pp = $data[0]=="" ? $data[1] : $data[0];
			if (metadata_exists('post', $post->ID, 'r2g_foundcity')){
	        	        update_post_meta($post->ID, 'r2g_foundcity', $pp);
		        } else {
		                add_post_meta($post->ID, 'r2g_foundcity', $pp);
        		}
		}
    	  }
	}
}

function wp_add_metakeys($val, $post){
	$meta_lat = '_ready2go_latitude';
	$meta_long = '_ready2go_longitude';
	if (metadata_exists('post', $post->ID, $meta_lat)){
		update_post_meta($post->ID, $meta_lat, $val[0]);
	} else {
		add_post_meta($post->ID, $meta_lat, $val[0]);
	}
	if (metadata_exists('post', $post->ID, $meta_long)){
                 update_post_meta($post->ID, $meta_long, $val[1]);
        } else {
                add_post_meta($post->ID, $meta_long, $val[1]);
        }
}

function get_location($val){
  $result = wp_remote_get("https://nominatim.openstreetmap.org/reverse?format=geocodejson&lat=" . $val[0] . "&lon=".$val[1]);
  if ($result!=false and !is_wp_error($result)){
        $result2 = (get_object_vars(json_decode($result['body'])))['features'][0]->properties->geocoding;
        $city = property_exists($result2, "city") ? $result2->city : "";
        $country = property_exists($result2, "country") ? $result2->country : "";
        return [$city, $country];
  } else {
	return [];
  }
}

?>
