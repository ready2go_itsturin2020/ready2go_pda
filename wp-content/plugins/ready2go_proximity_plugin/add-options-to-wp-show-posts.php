<?php

// file made by Gregoricchio Matteo

// adds a Location option to the 'Order By' Metabox of the wp-show-posts plugin.
// adds a Include Tags metabox to the  'Meta' section of wp-show-posts plugin.
function update_butterbean_manager_metaboxes () {

        $defaults = wpsp_get_defaults();
        $manager = butterbean()->get_manager( 'wp_show_posts' );

        $manager->unregister_control('wpsp_orderby');
        $manager->unregister_setting('wpsp_orderby');

        $manager->register_control(
                'wpsp_orderby', // Same as setting name.
                array(
                    'type'    => 'select',
                    'section' => 'wpsp_query_args',
                    'label'   => esc_html__( 'Order by', 'wp-show-posts' ),
                    'choices' => array(
                                        'none' => __( 'No order','wp-show-posts' ),
                                        'ID' => __( 'ID','wp-show-posts' ),
                                        'author' => __( 'Author','wp-show-posts' ),
                                        'title' => __( 'Title','wp-show-posts' ),
                                        'name' => __( 'Slug','wp-show-posts' ),
                                        'type' => __( 'Post type','wp-show-posts' ),
                                        'date' => __( 'Date','wp-show-posts' ),
                                        'modified' => __( 'Modified','wp-show-posts' ),
                                        'parent' => __( 'Parent','wp-show-posts' ),
                                        'rand' => __( 'Random','wp-show-posts' ),
                                        'comment_count' => __( 'Comment count','wp-show-posts' ),
                                        'location' => __('Location', 'wp-show-posts')
                                ),
                                'attr' => array( 'id' => 'wpsp-orderby' )
                )
            );

        $manager->register_setting(
                'wpsp_orderby', // Same as control name.
                array(
                    'sanitize_callback' => 'sanitize_text_field',
                                'default' => $defaults[ 'wpsp_orderby' ] ? $defaults[ 'wpsp_orderby' ] : 'date'
                )
            );

	$manager->register_control(
		'wpsp_include_tags',
		array(
			'type'        => 'checkbox',
			'section'     => 'wpsp_post_meta',
			'label'       => __( 'Include tags','wp-show-posts' ),
			'attr' => array( 'id' => 'wpsp-include-tags' )
		)
	);

	$manager->register_setting(
		'wpsp_include_tags',
		array(
			'sanitize_callback' => 'butterbean_validate_boolean',
			'default' => $defaults[ 'wpsp_include_tags' ] ? $defaults[ 'wpsp_include_tags' ] : false
		)
	);

	$manager->register_control(
	        'wpsp_tags_location', // Same as setting name.
	        array(
	            'type'    => 'select',
	            'section' => 'wpsp_post_meta',
	            'label'   => esc_html__( 'Tags location', 'wp-show-posts' ),
	            'choices' => array(
					'below-title' => __( 'Below title','wp-show-posts' ),
					'below-post' => __( 'Below post','wp-show-posts' )
				),
				'attr' => array( 'id' => 'wpsp-comments-link-location' )
	        )
	    );

	$manager->register_setting(
	        'wpsp_tags_location', // Same as control name.
	        array(
	            'sanitize_callback' => 'sanitize_text_field',
				'default' => $defaults[ 'wpsp_tags_location' ] ? $defaults[ 'wpsp_tags_location' ] : ''
	        )
	    );
}

// add default values for new metaboxes created (Include Tags)
function add_default_values_for_new_metaboxes ($defaults){
        $defaults['wpsp_tags_location'] = '';
        $defaults[ 'wpsp_include_tags' ] = false;
        return $defaults;
}

// updates the html data with tags when required personalized settings are set in "Meta" tab of wp-show-posts
// version for Below Title Requests
function personalized_settings_rendering_below_title( $settings ) {
        remove_action('wpsp_after_title','wpsp_add_post_meta_after_title');

        if ( ( $settings[ 'include_author' ] && 'below-title' == $settings[ 'author_location' ] )
	     || ( $settings[ 'include_date' ] && 'below-title' == $settings[ 'date_location' ] )
	     || ( $settings[ 'include_terms' ] && 'below-post' == $settings[ 'terms_location' ] )
             || ( $settings[ 'include_comments' ] && 'below-post' == $settings[ 'comments_location' ] )
             || ( $settings[ 'include_tags' ] && 'below-title' == $settings[ 'tags_location' ] ) ) {

		if (( $settings[ 'include_tags' ] && 'below-title' == $settings[ 'tags_location' ] )){
			add_filter('r2g_echo_tags_to_meta_below_t', 'tags_printout', 10, 1);
                }
	       	apply_filters('r2g_echo_tags_to_meta_below_t', wpsp_meta( 'below-title', $settings ));
        }
}

// updates the html data with tags when required personalized settings are set in "Meta" tab of wp-show-posts
// version for Below Post Requests
function personalized_settings_rendering_below_post ( $settings ) {
	remove_action( 'wpsp_after_content','wpsp_add_post_meta_after_content');

	if ( ( $settings[ 'include_author' ] && 'below-post' == $settings[ 'author_location' ] )
	     || ( $settings[ 'include_date' ] && 'below-post' == $settings[ 'date_location' ] )
	     || ( $settings[ 'include_terms' ] && 'below-post' == $settings[ 'terms_location' ] )
	     || ( $settings[ 'include_comments' ] && 'below-post' == $settings[ 'comments_location' ] )
	     || ( $settings[ 'include_tags' ] && 'below-title' == $settings[ 'tags_location' ] ) ) {
		if (( $settings[ 'include_tags' ] && 'below-post' == $settings[ 'tags_location' ] )){
                        add_filter('r2g_echo_tags_to_meta_below_p', 'tags_printout' , 10, 1);
                }
		apply_filters('r2g_echo_tags_to_meta_below_p', wpsp_meta( 'below-post', $settings ));
	}
}

// html render for Include TAGS
function tags_printout ( $data ) {
                $post_tags = get_the_tags();
                $separator = ' | ';
                $outpu ='';
                if (!empty($post_tags)) {
                        foreach ($post_tags as $tag) {
                            $outpu .= '<a class="url fn n" href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a>' . $separator;
                        }
                	$tags = trim($outpu, $separator);
			echo (sprintf( '<span class="wp-show-posts-terms wp-show-posts-meta">%1$s</span>', $tags ) ); 
                }
}

// add to setting variables the check for the selection of new metaboxes 
add_filter( 'wpsp_settings', 'add_metaboxes_to_setting_variables', 12);
function add_metaboxes_to_setting_variables ( $arr ) {
        $id = $arr['list_id'];
        $arr['include_tags'] = wp_validate_boolean( wpsp_get_setting( $id , 'wpsp_include_tags' ) );
        $arr['tags_location'] = sanitize_text_field( wpsp_get_setting( $id, 'wpsp_tags_location' ) );
        return $arr;
}


/*
 * first filter: called after butterbean metaboxes are registered to a butterbean manager.
 * 		  Retrieve the BB manager instance and updates Order By metabox with location option.
 *		  Add metaboxes option to Meta tab for Post Tags.
 * second filter: adds default values for Tags settings.
 * third/fourth filter:  overrides the default actions for Meta Content Rendering. It checks for Tags Requests and,
 * 		  if they're required, echoes them after "below post/title" section.
 * fifth filter: add metaboxes inputs to the wp-show-posts settings variables.
 */

add_action( 'butterbean_register', 'update_butterbean_manager_metaboxes', 13);
add_filter( 'wpsp_defaults', 'add_default_values_for_new_metaboxes', 12, 1 );
add_action( 'wpsp_after_title','personalized_settings_rendering_below_title', 9);
add_action( 'wpsp_after_content','personalized_settings_rendering_below_post', 9 );
add_filter( 'wpsp_settings', 'add_metaboxes_to_setting_variables', 12);


?>
