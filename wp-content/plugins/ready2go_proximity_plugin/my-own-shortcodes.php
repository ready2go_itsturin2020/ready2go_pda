<?php
// file made by Presutti Edoardo

//Reference URI: https://wp-buddy.com/blog/add-a-shortcode-to-every-post-or-page/
//Description:	Shortcode plugin that adds a special shortcode to a post, page or custom post type.
//		!Adapted from wp-buddy version to print a ResponsiveVoice button on every post content!
//Author: wp-buddy.org (ADAPTED FROM)

function my_shortcode_to_a_post( $content ) {
  global $post;
  if( ! $post instanceof WP_Post ) return $content;

  switch( $post->post_type ) {
    case 'post':
    	return '[responsivevoice_button voice="Italian Female"] ' . $content;

    default:
      return $content;
  }
}

add_filter( 'the_content', 'my_shortcode_to_a_post' );
?>
