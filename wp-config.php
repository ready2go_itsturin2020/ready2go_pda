<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
//define( 'DB_NAME', 'wp_ready2go' );
define('DB_NAME', 'wp_ready2go');

/** MySQL database username **/
define( 'DB_USER', 'ready2go' );

/** MySQL database password */
define( 'DB_PASSWORD', 'YUaz7sLL' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1+Hcb+;5x+-+R`X%q`,2CZ=n,8xLHo 8{Ce,-HE%R?> -5o[_(XH                                  [;/z+xdrZ<3%');
define('SECURE_AUTH_KEY',  '|ot&46[-M4RsC}6X,S&`h*Th-p>Ch-r&>h`y]$<DYgg8...>+~1g                                  0#%/FL`.-TY]');
define('LOGGED_IN_KEY',    'l|Pd2Zf=7z;|aE@q=.Ci)Y_<[-9I,64?W`bDceP(hm!+|_,^tcPD                                  )T<WyS?}:^Zj');
define('NONCE_KEY',        'SMWA:9`<bXuGSii@jUl6xa8-^dd0|gr<,Vz8eR<KfkUmE0P(;hRx                                  p$e6sQ^wC_af');
define('AUTH_SALT',        ';g-+HeKG!7ruYMbtj/t-gjRa)P2+`x8f_JcF!`#m-hsF2:Vj9gXU                                  2+=r-cK^c%,2');
define('SECURE_AUTH_SALT', 'r3=Z`=l{Is(.&RsX5IX>,&F&ZN>p*D|Oi|ywBz^dC^lHN&rSe->U                                  t-nBYkH{W?T3');
define('LOGGED_IN_SALT',   '+LLx95A_[|)4_)+>%|=:<-X t_yTdb)ueez7j)wp$>C]NU6(9?O2                                  2|yb{-}}#cly');
define('NONCE_SALT',       ':T?sT5M{g$^%&hP-6.p-]g+^42^fA0|{.k>YW9DU8;+7eZ0<-#6y                                  2P -&8T-!BRv');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );
//define( 'WP_DEBUG', true );
define('WP_DEBUG_LOG', true);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
